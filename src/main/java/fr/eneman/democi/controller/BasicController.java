package fr.eneman.democi.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * BasicController
 */
@RestController
public class BasicController {

    @GetMapping(value = "/")
    public String sayHello() {
        return "<h1>Bye bye</h1>";
    }

}
