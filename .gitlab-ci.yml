stages:
  - test
  - build
  - docker
  - deploy

variables:
  MAVEN_OPTS: "-XX:MaxPermSize=512m -Xmx1536m -Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true -Dmaven.artifact.threads=1"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  JAVA_HOME: "/usr/lib/jvm/java-11-openjdk-amd64/"
  MAVEN_BIN: "mvn"
  GIT_SSL_NO_VERIFY: 1

validate-source:
  stage: test
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS validate"
  image: maven:3.6.0-jdk-11

test:
  stage: test
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS -V verify"
  image: maven:3.6.0-jdk-11

package:jdk11:
  stage: build
  image: maven:3.6.0-jdk-11
  variables:
    GIT_SSL_NO_VERIFY: 1
    JAVA_HOME: /docker-java-home
  script:
    - "$MAVEN_BIN $MAVEN_CLI_OPTS  -V package -DskipTests"
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 day

docker:
  stage: docker
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:19.03.1-dind
  image: docker:19.03.1
  script:
    - docker build --pull -t "donatien26/demo-ci:$CI_COMMIT_SHA" .
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
    - docker push "donatien26/demo-ci:$CI_COMMIT_SHA"

#######################################################################
#                   Deployer sur kub                                  #
#######################################################################
deploy review on kub:
  stage: deploy
  variables:
    APP_ID: demo-$CI_COMMIT_REF_SLUG
    HAPROXY: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.dev.insee.io
    REVIEW_IMAGE: donatien26/demo-ci:$CI_COMMIT_SHA
  image:
    name: thisiskj/kubectl-envsubst
    entrypoint: ["/bin/sh", "-c"]
  script:
    - cd .ci/kubernetes/
    - envsubst < app.yml
    - envsubst < app.yml | kubectl --token $KUB_TOKEN --server $KUB_SERVER --namespace $KUB_NAMESPACE --insecure-skip-tls-verify=true replace --force -f -
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.dev.insee.io
    on_stop: stop_review on kub

stop_review on kub:
  stage: deploy
  variables:
    GIT_STRATEGY: none
    APP_ID: demo-$CI_COMMIT_REF_SLUG
    HAPROXY: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.dev.insee.io
    REVIEW_IMAGE: donatien26/demo-ci:$CI_COMMIT_SHA
  image:
    name: thisiskj/kubectl-envsubst
    entrypoint: ["/bin/sh", "-c"]
  script:
    - envsubst < app.yml | kubectl --token $KUB_TOKEN --server $KUB_SERVER --namespace $KUB_NAMESPACE --insecure-skip-tls-verify=true delete -f -
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop

#######################################################################
#                   Deployer sur Marathon                             #
#######################################################################
deploy review on Marathon:
  stage: deploy
  variables:
    APP_ID: demo-$CI_COMMIT_REF_SLUG
    HAPROXY: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.demo.dev.sspcloud.fr
    REVIEW_IMAGE: donatien26/demo-ci:$CI_COMMIT_SHA
  image: rija/docker-alpine-shell-tools
  script:
    - "envsubst < .ci/marathon/marathon.json"
    - 'envsubst < .ci/marathon/marathon.json | curl -d@- -H "Content-Type: application/json" -X PUT http://deploy.alpha.innovation.insee.eu/v2/apps/$APP_ID?force=true'
    - "curl -X POST http://deploy.alpha.innovation.insee.eu/v2/apps/$APP_ID/restart?force=true"
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_PROJECT_ID-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.dev.innovation.insee.eu
    on_stop: stop_review on marathon

stop_review on marathon:
  stage: deploy
  variables:
    GIT_STRATEGY: none
    APP_ID: demo-$CI_COMMIT_REF_SLUG
  script:
    - "curl -X DELETE http://deploy.alpha.innovation.insee.eu/v2/apps/$APP_ID"
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop

modify argo conf:
  stage: deploy
  image: alpine:3.8
  before_script:
    - apk add --no-cache git curl bash
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  > kustomize.sh
    - ./kustomize.sh
    - mv kustomize /usr/local/bin/
    - git clone https://gitlab.com/Donatien26/demo-ci-conf.git
    - cd demo-ci-conf
    - git remote set-url origin https://${CI_GIT_USER}:${CI_GIT_PASSWORD}@gitlab.com/Donatien26/demo-ci-conf.git
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - git checkout -b update-image-$CI_COMMIT_SHA
    - cd overlays/dev
    - kustomize edit set image donatien26/demo-ci:$CI_COMMIT_SHA
    - cat kustomization.yaml
    - git commit -am "update image update to donatien26/demo-ci:${CI_COMMIT_SHA}"
    - git push origin update-image-$CI_COMMIT_SHA -o merge_request.create
  only:
    - master
